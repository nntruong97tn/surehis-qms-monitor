import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { QmsServiceService } from '../common/qms-service.service';
import { ModalConfig } from '../model/ModalConfig';
import { Router } from '@angular/router';

@Component({
  selector: 'app-config-qms',
  templateUrl: './config-qms.component.html',
  styleUrls: ['./config-qms.component.scss']
})
export class ConfigQmsComponent implements OnInit {
  public data: any=[
    {id:1,name:"Phòng"},
    {id:2,name:"Quầy"}
  ];
  public valueText:number=0
  public valueType:number;
  private mdConfig:ModalConfig=new ModalConfig();
  constructor(private qmsSvr:QmsServiceService, public router: Router) {
    
   }
  public date:any='';
  
  ngOnInit() {
    this.loadData();
    const now = moment().locale('vi');
    this.date=now.format('dddd') + ', ngày '+ now.format('L'); 
  }

  loadData(){
    this.mdConfig=this.qmsSvr.getConfig();
    this.valueType=this.mdConfig.TypeLocation;
    this.valueText=this.mdConfig.ValueLocation
    
  }

  valueChange(event){
  }

  onChange(event){    
  }

  SetConfig(){
    this.mdConfig.TypeLocation=this.valueType
    this.mdConfig.ValueLocation=this.valueText
    this.qmsSvr.setConfig(this.mdConfig)
    this.router.navigate(['/dashboard']);
    
  }

}
