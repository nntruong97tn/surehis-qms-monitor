import { Injectable } from '@angular/core';
import { ModalConfig } from '../model/ModalConfig';

@Injectable({
  providedIn: 'root'
})
export class QmsServiceService {
  private mdConfig:ModalConfig=new ModalConfig();
  constructor() { }
  
  setConfig(data:ModalConfig){
    this.mdConfig=data;
    this.setCookie('config',JSON.stringify(this.mdConfig),7);
  }
  getConfig():ModalConfig{
    let cf: ModalConfig;
    let data=this.getCookie('config');
    cf=JSON.parse(data);
    return cf;
  }

  setCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
  }
  getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return JSON.stringify(new ModalConfig());
  }
}
